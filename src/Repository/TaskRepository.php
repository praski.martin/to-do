<?php

namespace App\Repository;

use App\Entity\Task;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Task|null find($id, $lockMode = null, $lockVersion = null)
 * @method Task|null findOneBy(array $criteria, array $orderBy = null)
 * @method Task[]    findAll()
 * @method Task[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TaskRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Task::class);
    }

    public function getTasksByUser(User $user): array
    {
        return $this->createQueryBuilder('task')  // zwraca mi taski
            ->where('task.done = false')  // muszą być nie wykonanae
            ->andWhere('task.responsibleUser = :user or task.responsibleUser is null')
            ->setParameter('user',$user)  //wrzucam zmienna do sql przekazana jako parametr
            ->orderBy('task.date','ASC')
            ->getQuery()
            ->getResult();
    }

    public function getCompletedTasksByUser(User $user): array
    {
        return $this->createQueryBuilder('completedTasks')
            ->where('completedTasks.done = true')
            ->andWhere('completedTasks.responsibleUser = :user or completedTasks.responsibleUser is null')
            ->setParameter('user',$user)
            ->getQuery()
            ->getResult();
    }
}
