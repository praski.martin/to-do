<?php

namespace App\Controller;

use App\Entity\Task;
use App\Entity\User;
use App\Form\TaskType;
use App\Repository\TaskRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/task")
 */
class TaskController extends AbstractController
{
    /**
     * @Route("/", name="task_index", methods={"GET"})
     */
    public function index(TaskRepository $taskRepository): Response
    {
        /** @var User $user */  //w tej zmiennej jest user

        $user = $this->getUser();
        $admin = $this->isGranted('ROLE_ADMIN');

        if ($admin){
            return $this->render('task/index.html.twig',[
                'tasks' => $taskRepository->findBy(['done'=> false],['date' => 'ASC']),
                'completedTasks' => $taskRepository->findBy(['done' => true],['date' => 'ASC']),
            ]);
        }else{
            return  $this->render('task/index.html.twig',[
                'tasks' => $taskRepository->getTasksByUser($user),
                'completedTasks' => $taskRepository->getCompletedTasksByUser($user),
            ]);
        }

    }

    /**
     * @Route("/new", name="task_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $task = new Task();
        $form = $this->createForm(TaskType::class, $task);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($task);
            $entityManager->flush();

            return $this->redirectToRoute('task_index');
        }

        return $this->render('task/new.html.twig', [
            'task' => $task,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="task_show", methods={"GET"})
     */
    public function show(Task $task): Response
    {
        return $this->render('task/show.html.twig', [
            'task' => $task,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="task_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Task $task): Response
    {
        $form = $this->createForm(TaskType::class, $task);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            return $this->redirectToRoute('task_index');
        }

        return $this->render('task/edit.html.twig', [
            'task' => $task,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}/done", name="task_done")
     */
    public function done(Task $task):Response
    {
        $task->setDone(true);
        $this->getDoctrine()->getManager()->flush();

        return $this->redirectToRoute('task_index');
    }

    /**
     * @Route("/{id}/not_done", name="not_done")
     */
    public function notRealized(Task $task):Response
    {
        $task->setDone(false);
        $this->getDoctrine()->getManager()->flush();

        return $this->redirectToRoute('task_index');
    }

    /**
     * @Route("/{id}", name="task_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Task $task): Response
    {
        if ($this->isCsrfTokenValid('delete'.$task->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($task);
            $entityManager->flush();
        }

        return $this->redirectToRoute('task_index');
    }
}
